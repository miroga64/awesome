<?php include_once './header_static.php'; ?>
<div class="form entry">
    <div class="form__title">Вход</div>
    <form>
        <label class="form__label">
            <div class="form__label__title">E-mail</div>
            <input type="text" placeholder="evgeny.petrov@gmail.com">
        </label>
        <div class="form__label">
            <div class="form__label__title">Пароль</div>
            <input type="password">
            <div class="form__info">
                <div class="form__info__icon"></div>
                <div class="form__info__hover">
                    <div class="form__info__title">Пароль должен содержать:</div>
                    <ul class="form__info__list">
                        <li>– Латинские буквы (A-Z)</li>
                        <li>– Цифры (0-9)</li>
                        <li>– Символы (?-!)</li>
                    </ul>
                </div>
            </div>
        </div>
        <input type="submit" value="Войти">
        <a href="/forgot.php" class="come_in"> Забыли пароль?</a>
        <a href="/" class="come_in"> Регистрация</a>
    </form>
</div>
<?php include_once './footer.php'; ?>
let gulp = require('gulp'),
	concat = require('gulp-concat'),
	// uglyfly = require('gulp-uglyfly'),
	include = require('gulp-include'),
	sass = require('gulp-sass'),
	sassGlob = require('gulp-sass-glob');

sass.compiler = require('node-sass');

let template_path = './';

let paths = {
	js: {
		watch: template_path+ 'dev/js/**/*.js',
		src: template_path+ 'dev/js/*.js',
		dest: template_path
	},
	sass: {
		src: template_path+ 'dev/scss/**/*.scss',
		dest: template_path
	}
};

function js() {
	return gulp.src(paths.js.src)
		// .pipe(concat("scripts.js"))
		// .pipe(uglyfly())
		.pipe(include())
		.pipe(gulp.dest(paths.js.dest));
}

function scss() {
	return gulp.src(paths.sass.src)
		.pipe(sassGlob())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest(paths.sass.dest));
}

function watch() {
	gulp.watch(paths.js.watch, js);
	gulp.watch(paths.sass.src, scss);
}

// let build = gulp.series(gulp.parallel(scss, js));

exports.watch = watch;
// exports.default = build;
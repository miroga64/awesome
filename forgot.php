<?php include_once './header_static.php'; ?>
<div class="form entry forgot">
    <div class="form__title">Забыли пароль?</div>
    <div class="form__text">Введите адрес электронной почты, который вы использовали при регистрации и мы вышлем вам инструкцию по восстановлению пароля.</div>
    <form>
        <label class="form__label">
            <div class="form__label__title">E-mail</div>
            <input type="text" placeholder="evgeny.petrov@gmail.com">
        </label>
        <input type="submit" value="Восстановить пароль">
        <a href="/entry.php" class="come_in">Назад</a>
    </form>
</div>
<?php include_once './footer.php'; ?>
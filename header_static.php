<!DOCTYPE html>

<html lang="ru">

<head>
    <?php 

    $title = isset($title) ? $title : 'Главная';

    function is_main() {
        return $_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php' ? true : false;
    }

    ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, , initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src='https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css' rel='stylesheet' />
    
    <title><?php echo $title; ?></title>

    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="styles.css" type="text/css">

    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="jquery-migrate-1.2.1.min.js"></script>

    <script src="plugins.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>

</head>

<body>

<header class="header">
   <div class="header__logo"></div>
   <div class="header__user"></div>
</header>

<main class="main static">
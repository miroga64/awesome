<?php include_once './header.php'; ?>
    <div class="yandex">
        <div class="yandex__title">Yandex SK Pro</div>
        <div class="yandex__container">
            <div class="yandex__tabs">
                <div class="yandex__tabs__element active" data-tab="1">Распознавание</div>
                <div class="yandex__tabs__element" data-tab="2">Синтез</div>
            </div>
            <div class="yandex__content active" data-tab="1">
                <form>
                    <div class="yandex__first_tab active">
                        <div class="yandex__describtion">Нажми «Добавить», выбери нужный файл и отправь</div>
                        <div class="yandex__file__label">
                            <input type="file">
                            <div class="yandex__file">
                                <div class="add">Добавить</div>
                                <span>+</span>
                            </div>
                        </div>
                    </div>
                    <input class="submit__tab_1" type="submit" value="Отправить" disabled>
                </form>
                <div class="second_slide">
                    <div class="yandex__describtion">Файл обрабатывается …</div>
                    <div class="yandex__load__container">
                        <div class="yandex__load"></div>
                    </div>
                </div>
                <div class="third_slide">
                    <textarea class="textarea__tab_1">В этом занятии мы разберемся с первой и основной вкладкой утилит хром, вкладкой элемент. Для перехода в данный раздел необходимо открыть сайт, перейти в пункт меню элемент, далее нажать кнопку начать, кл+икнуть правой кнопкой выбрать инспект и просмотреть код. Вкладка элемент отображает структуру нашей страницы в виде HTML. Чтобы понимать происходящее, сделаем небольшой экскурс в описание «Что такое HTML». Как вы наверное знаете, любая веб-страница это HTML-код, который преобразуется нашим браузером в то, что мы привыкли видеть. Если пр+осто - HTML говорит браузеру о том, какие элементы должны присутствовать на странице. HTML (от английского hypertext markup language) — это стандартизированный язык разметки документов во всемирной паутине. Большинство веб страниц содержит описание разметки на этом языке. Язык HTML интерпретируется браузером. Полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства. HTML - это подвид xml, следовательно, он строится по тем же правилам и они достаточно просты.</textarea>
                    <div class="btn_back" type="submit">Заново</div>
                </div>
                <!-- <div class="yandex__load__container">
                    <div class="yandex__load"></div>
                </div> -->
            </div>
            <div class="yandex__content" data-tab="2">
                <form>
                    <div class="yandex__first_tab">
                        <div class="yandex__describtion">Добавь текст в поле и отправь</div>
                        <textarea class="textarea__tab_1">В этом занятии мы разберемся с первой и основной вкладкой утилит хром, вкладкой элемент. Для перехода в данный раздел необходимо открыть сайт, перейти в пункт меню элемент, далее нажать кнопку начать, кл+икнуть правой кнопкой выбрать инспект и просмотреть код. Вкладка элемент отображает структуру нашей страницы в виде HTML. Чтобы понимать происходящее, сделаем небольшой экскурс в описание «Что такое HTML». Как вы наверное знаете, любая веб-страница это HTML-код, который преобразуется нашим браузером в то, что мы привыкли видеть. Если пр+осто - HTML говорит браузеру о том, какие элементы должны присутствовать на странице. HTML (от английского hypertext markup language) — это стандартизированный язык разметки документов во всемирной паутине. Большинство веб страниц содержит описание разметки на этом языке. Язык HTML интерпретируется браузером. Полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства. HTML - это подвид xml, следовательно, он строится по тем же правилам и они достаточно просты.
                        </textarea>
                    </div>
                    <div class="yandex_subtitle">Для передачи слов-омографов, используйте «+» перед ударной гласной: з+амок, зам+ок.
Чтобы отметить паузу между словами используйте «-».</div>
                    <input class="submit__tab_2" type="submit" value="Отправить">
                </form>
                <div class="second_slide_2">
                    <div class="yandex__describtion">Файл обрабатывается …</div>
                    <div class="yandex__load__container">
                        <div class="yandex__load"></div>
                    </div>
                </div>
                <div class="third_slide_3">
                    <div class="yandex__describtion">Нажми «Добавить», выбери нужный файл и отправь</div>
                    <div class="yandex__file__label">
                        <input type="file">
                        <div class="yandex__file">
                            <div>tts.ogg</div>
                            <span class="dowload"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('body').on('click','[data-tab="1"] div.add', function(e){
                e.stopPropagation();
                $('[data-tab="1"] .yandex__file__label input').trigger('click')
            })

            $('[data-tab="1"] .yandex__file__label input').on('change', function(e){
                e.stopPropagation();
                let name = $(this).val().split('\\')[2];
                $('[data-tab="1"] .yandex__file').text('')
                $('[data-tab="1"] .yandex__file').append(`
                    <div class="add">${name}</div>
                    <span class="delete"></span>
                `)
                $('.submit__tab_1').prop('disabled', false)
            })

            $('body').on('click', '[data-tab="1"] span.delete', function(e){
                e.stopPropagation();
                $('[data-tab="1"] .yandex__file__label input').val('')
                $('[data-tab="1"] .yandex__file').text('')
                $('[data-tab="1"] .yandex__file').append(`
                    Добавить
                    <span>+</span>
                `)
                $('.submit__tab_1').prop('disabled', true)
            })
            $('.yandex__tabs__element[data-tab]').on('click', function(){
                $('[data-tab].active').removeClass('active');
                $(`[data-tab=${$(this).attr('data-tab')}]`).addClass('active')
            })
            $('.submit__tab_1').on('click', function(e){
                e.preventDefault();
                $(this).parent().addClass('disabled');
                $('.second_slide').addClass('active')
                setTimeout(() => {
                    $('.second_slide').removeClass('active');
                    $('.third_slide').addClass('active');
                }, 3000);
            })

            $('.submit__tab_2').on('click', function(e){
                e.preventDefault();
                $(this).parent().addClass('disabled');
                $('.second_slide_2').addClass('active')
                setTimeout(() => {
                    $('.second_slide_2').removeClass('active');
                    $('.third_slide_3').addClass('active');
                }, 3000);
            })
            
        })
    </script>
<?php include_once './footer.php'; ?>
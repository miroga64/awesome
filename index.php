<?php include_once './header.php'; ?>
<div class="form registration">
    <div class="form__title">Регистрация</div>
    <form>
        <label class="form__label">
            <div class="form__label__title">* Логин</div>
            <input type="text" placeholder="helloworld">
        </label>
        <label class="form__label">
            <div class="form__label__title">* Имя</div>
            <input type="text" placeholder="Евгений Петров">
        </label>
        <label class="form__label">
            <div class="form__label__title">* E-mail</div>
            <input type="text" placeholder="evgeny.petrov@gmail.com">
        </label>
        <div class="form__label">
            <div class="form__label__title">* Пароль</div>
            <input type="password" placeholder="">
            <div class="form__switcher hidden"></div>
        </div>
        <div class="form__label">
            <div class="form__label__title">* Повторите пароль</div>
            <input type="password">
            <div class="form__info">
                <div class="form__info__icon"></div>
                <div class="form__info__hover">
                    <div class="form__info__title">Пароль должен содержать:</div>
                    <ul class="form__info__list">
                        <li>– Латинские буквы (A-Z)</li>
                        <li>– Цифры (0-9)</li>
                        <li>– Символы (?-!)</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="form__more">
            <div class="form__more__container">
                <label class="form__label">
                    <div class="form__label__title">Фамилия</div>
                    <input type="text" placeholder="Петров">
                </label>
                <label class="form__label">
                    <div class="form__label__title">Возраст</div>
                    <input type="text" placeholder="21 год">
                </label>
                <label class="form__label">
                    <div class="form__label__title">Пол</div>
                    <select>
                        <option value="Male">Мужской</option>
                        <option value="Female">Женский</option>
                    </select>
                </label>
                <label class="form__label">
                    <div class="form__label__title">Город</div>
                    <select>
                        <option value="Male">Уфа</option>
                        <option value="Female">Другие города</option>
                    </select>
                </label>
                <label class="form__label">
                    <div class="form__label__title">Телефон</div>
                    <input type="text" placeholder="+7(800) 555-55-55">
                </label>
            </div>
            <div class="form__more__title"></div>
        </div>
        <input type="submit" value="Зарегистрироваться">
        <a href="/entry.php" class="come_in">Войти</a>
    </form>
</div>
<?php include_once './footer.php'; ?>
<?php include_once './header_application.php'; ?>
    <?php include_once './components/side_menu.php'; ?>
    <div class="page">
        <div class="text" id="container2">
            <div class="page__menu">
                <div class="page__menu__left">
                    <div class="page__menu__element active">
                        <div class="page__menu__element__icon" style="background-image: url(./images/fixed__text.svg);"></div>
                        <span>Текст</span>
                    </div>
                    <div class="page__menu__element">
                        <div class="page__menu__element__icon" style="background-image: url(./images/fixed__headphone.svg);"></div>
                        <span>Аудио</span>
                    </div>
                    <div class="page__menu__element">
                        <div class="page__menu__element__icon" style="background-image: url(./images/fixed__play.svg);"></div>
                        <span>Видео</span>
                    </div>
                </div>
                <div class="page__menu__back">Назад</div>
            </div>
            <div class="page__fixed">
                <div class="page__fixed__left">
                    <div class="page__fixed__menu"></div>
                    <div class="page__fixed__divide">
                        <div class="page__fixed__element active">
                            <div class="page__fixed__element__icon" style="background-image:url(./images/fixed__text.svg)"></div>
                        </div>
                        <div class="page__fixed__element">
                            <div class="page__fixed__element__icon" style="background-image:url(./images/fixed__headphone.svg)"></div>
                        </div>
                        <div class="page__fixed__element">
                            <div class="page__fixed__element__icon" style="background-image:url(./images/fixed__play.svg)"></div>
                        </div>
                    </div>
                </div>
                <div class="page__fixed__right">Назад</div>
                <div class="page__fixed__scroll"></div>
            </div>
            <div class="text__content">
                <div class="text__image" style="background-image:url(./images/banner_text.png)"></div>
                <div class="text__title">
                    <div class="text__title__text">
                        Руководство по базе Photoshop для дизайнеров
                    </div>
                    <div class="text__title__menu"></div>
                </div>
                <div class="text__container">
                    <div class="text__info">Если вы достаточно быстро печатаете и сможете придумать много броских заголовков, вам пора писать лонгриды. Шутка. Кстати, как правильно — лонгриды или лонграйты? </div>
                    <div class="text__info">Лонгриды — они не про количество тыщезнаков и не про продающие формулы текста. Не про то, как правильно подобрать картинки к материалу. И не про количество просмотров, конечно. И даже не про то, как изучить потребности ЦА и попасть текстом в них. Это самый амбициозный, бросающий вызов автору формат контента. И он — совсем не то, что роится в головах у нынешних авторов.</div>
                    <h2 class="text__subtitle">Это лучший формат для долгосрочных инвестиций&nbsp;в контент</h2>
                    <div class="text__info">Формально любой объемистый материал, который хорошо сверстан и наполнен разнообразным содержимым (текст, фото, интерактивный контент), — это и есть лонгрид. Да, он длинный — считают, что от 1200 слов или восьми тысяч знаков. Но таким ценным его делает не размер и не навороченная верстка, а определенный подход к работе над материалом. Сделайте текст меньше, чем 1200 слов — он останется лонгридом. Уберите верстку — он останется лонгридом. Убейте глубину освоения темы и авторскую подачу — все пропало. Не зря в англоязычной традиции лонгрид еще называется deep read.</div>
                    <img src="./images/marks_page.png" class="text__img">
                    <div class="text__describtion">Классика: первый сайт Марка Цукерберга. О каких лонгридах вы говорите?</div>
                    <div class="text__info">А потом появился HTML 5. Началось переложение жанра на цифровую среду и его адаптация под маркетинговые нужды. На отрезке времени с появления HTML 5 до 2016 года есть две важных точки. Первая — легендарный Snow Fall. В 2012 году команда The New York Times опубликовала мультимедийный материал о лыжниках и сноубордистах, оказавшихся в снежной ловушке. Хотя за прошедшие четыре года появились материалы куда более впечатляющие, «Снегопад» стал первым, поэтому он останется маяком, и в 2025 году о нем будут рассказывать студентам-медийщикам, как когда-то рассказывали о концепциях Маршалла Маклюэна.</div>
                    <div class="text__info">Вторая точка — «Почему я купил дом в Детройте за 500 долларов». В 2014 году ребята в BuzzFeed опубликовали историю человека, который рискнул и купил дом на аукционе сомнительной недвижимости в городе, где белый парень на улице выглядит более чем странно. История набрала 1 млн 775 тысяч 125 просмотров к декабрю 2016 года. Количество прочтений продолжает расти до сих пор — текст живет.</div>
                    <div class="text__slider__container">
                        <div class="text__slider">
                            <div class="text__slider__element" style="background-image:url('./images/text_slider.png')"></div>
                            <div class="text__slider__element" style="background-image:url('./images/text_slider.png')"></div>
                            <div class="text__slider__element" style="background-image:url('./images/text_slider.png')"></div>
                        </div>
                        <div class="text__slider__navigation">
                            <div class="text__slider__dots"></div>
                        </div>
                    </div>
                    <div class="text__info">В «Снегопаде» 18 тысяч знаков, в «Доме в Детройте» — 35 тысяч, причем больше половины пользователей, прочитавших «Дом в Детройте», делали это с мобильного устройства.</div>
                    <div class="text__info">Это очень, очень важные точки. Первая — «Снегопад» — всегда должна напоминать нам, что создание успешного контента происходит на стыке журналистики, читайте «серьезной работы с фактом, формой и авторским мнением», и дизайна, который знает, как именно мы юзаем эти простыни текста. Только верстка и мультимедийный контент не сделают дурной длинный текст хорошим лонгридом. Вторая точка — «Дом в Детройте» — говорит о том, что даже ребята, которые любят одноразовый баззфидный контент, готовы читать длинные тексты. У людей появился вкус к историям, и деньги, которые компании тратят на контент, могут и должны стать долгосрочными инвестициями, а не карманными расходами.</div>
                    <div class="text__info">В какой контент вкладывались деньги до этого? Продающие письма, seo, «мегаполезный-оставьте-на-стене» контент для соцсетей, лендинги. Все это либо мертво, либо при смерти. Лонгриды жизнеспособны. </div>
                    <div class="text__info">Лонгриды не умрут, потому что их нельзя конструировать за счет простейших форм воздействия, как в случае с лендингами. Ими невозможно объесться до тошноты, как продающими текстами, построенными на ограниченном наборе приемов. Никакого волшебства: просто авторы снова стали серьезно работать над содержанием.</div>
                    <div class="text__info">Дизайн не умрет завтра, журналистика не умрет завтра. Желание сопереживать чьей-либо истории тоже не умрет завтра. Это именно те вещи, на которых стоит лонгрид, а значит, это лучший жанр для долгосрочных инвестиций в контент.</div>
                    <div class="text__btn__container">
                        <div class="text__btn">Непонятно</div>
                        <div class="text__btn dark">Далее</div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
<?php include_once './components/modules_menu.php';?>
<?php include_once './footer_application.php'; ?>
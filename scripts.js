$(document).ready(function(){
    const container = document.querySelector('#container');
    const container2 = document.querySelector('#container2');


    if(container){
        Ps.initialize(container);
    }

    if(container2){
        Ps.initialize(container2);
    }
    
    let element = 0

    $('.form__more__title').on('click', function(){
        $(this).prev().toggleClass('active');
        $(this).parent().toggleClass('active');
        if($(this).parent().hasClass('active')){
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".form__more__title").offset().top - 100
            }, 400);
        }else{
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".form__switcher").offset().top - 530
            }, 200);
        }
    })

    $('.form__switcher').on('click', function(){
        $(this).toggleClass('hidden');
        if($(this).hasClass('hidden')){
            $(this).prev().attr('type', 'password')
        }else{
            $(this).prev().attr('type', 'text')
        }
    })

    $('.skills__edit').on('click', function(){
        $(this).toggleClass('active');
        $('.skills__element').toggleClass('show');
    })

    $('body').on('click', '.skills__element.active:not(".show")' , function(){
        $('.add_modules.menu').addClass('active');
    })

    $('body').on('click','.skills__element.show', function(){
        element = $(this)
        $('.add_modules.add').addClass('active');
    })

    $('.add_modules__cross').on('click', function(){
        $('.add_modules').removeClass('active');
    })

    $('.add_modules__bg').on('click', function(){
        $(this).parent().find('.add_modules__cross').trigger('click');
    })

    $('.page__fixed__menu').on('click', function(){
        $('.add_modules.menu').addClass('active');
    })
    $('.text__title__menu').on('click', function(){
        $('.add_modules.menu').addClass('active');
    })

    $('.add_modules__element').on('click', function(){
        element.addClass('active');
        element.find('.skills__element__icon').css('background-image', $(this).attr('data-image'));
        element.append(`<div class="skills__element__cross"></div>`)
        element.append(` <div class="skills__element__hover">
        <div class="skills__element__text">Подавление агрессии в коллективе</div>
        <div class="skills__element__count">4/20</div>
        <div class="skills__element__checked"></div>
    </div>`)
        $('.add_modules__cross').trigger('click');
    })

    $('body').on('click', '.skills__element__cross', function(e){
        e.stopPropagation();
        $(this).parent().removeClass('active');
    })

    $('.text__slider').slick({
        slidesToShow: 1,
        prevArrow:'<div class="text__slider__arrow"></div>',
        nextArrow:'<div class="text__slider__arrow rotate"></div>',
        appendArrows: $('.text__slider__navigation'),
        dots: true,
        appendDots: $('.text__slider__dots'),
        responsive:[
            {
                breakpoint: 575,
                settings:{
                    arrows:false,

                }
            }
        ]
    })

    if($('.text').length > 0){
        let full_leng = 0
        $('.text__content').children().each(function(index){
            full_leng = full_leng + $($('.text__content').children()[index]).innerHeight();
        })
        $('.text').scroll(function(){
            let scroll_ost = full_leng - $(window).innerHeight();
            let cof = $('.text').scrollTop() / scroll_ost;
            $('.page__fixed__scroll').css('width', (cof * 100) + '%')
            
            if(($('.text').scrollTop() > 50) && !$('.page__fixed').hasClass('.active')){
                $('.page__fixed').addClass('active');
            }else{
                $('.page__fixed.active').removeClass('active');
            }
        })

       
    }
    $('li[data-content]').on('click', function(){
        $(this).parent().parent().append(`<div class="music__menu__popup__text">Спасибо за помощь в борьбе с контентом для взрослых.
        Мы изучим материалы и сообщим о некачественном контенте автору.</div>` )
        $(this).parent().remove();
    })

    $('.music__menu').on('click', function(){
        $(this).addClass('active');
    })
    $('.music__menu__exit').on('click', function(e){
        e.stopPropagation();
        $('.music__menu').removeClass('active');
    })

    $('.music__mobile__title').on('click', function(){
        $('.music__mobile').addClass('active');
        $('.music__mobile__bg').addClass('active');
    })
    $('.music__mobile__title__arrow').on('click', function(e){
        e.stopPropagation();
        $('.music__mobile').removeClass('active');
        $('.music__mobile__bg').removeClass('active');
    })  
    $('.music__mobile__title__exit').on('click', function(e){
        e.stopPropagation();
        $('.music__mobile').addClass('disabled');
        $('.music__mobile__bg').removeClass('active');
    })

    $('.add_modules__slider').slick({
        slidesToShow: 4,
        infinite: false,
        prevArrow:'<div class="add_modules__arrow"></div>',
        nextArrow:'<div class="add_modules__arrow rotate"></div>',
        responsive:[
            {
                breakpoint: 1060,
                settings:{
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 826,
                settings:{
                    slidesToShow: 2,
                    variableWidth: true,
                } 
            }, 
            {
                breakpoint: 575,
                settings:{
                    arrows:false,
                    slidesToShow: 2,
                    variableWidth: true,
                } 
            }
        ]
    })
})

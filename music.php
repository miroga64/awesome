<?php include_once './header_application.php'; ?>
    <?php include_once './components/side_menu.php'; ?>
    <div class="page">
        <div class="music">
            <div class="page__menu">
                <div class="page__menu__left">
                    <div class="page__menu__element">
                        <div class="page__menu__element__icon" style="background-image: url(./images/fixed__text.svg);"></div>
                        <span>Текст</span>
                    </div>
                    <div class="page__menu__element active">
                        <div class="page__menu__element__icon" style="background-image: url(./images/fixed__headphone.svg);"></div>
                        <span>Аудио</span>
                    </div>
                    <div class="page__menu__element">
                        <div class="page__menu__element__icon" style="background-image: url(./images/fixed__play.svg);"></div>
                        <span>Видео</span>
                    </div>
                </div>
                <div class="page__menu__back">Назад</div>
            </div>
            <div class="music__picture">
                <h1 class="music__picture__title">Начало работы в&nbsp;Treelone</h1>
                <div class="music__picture__img" style="background-image:url(./images/music_bg.png)">

                </div>
                <div class="music__picture__play">

                </div>
            </div>
            <div class="music__playlist" id="container">
                <div class="music__element active">
                    <div class="music__logo" style="background-image: url(./images/add_modules_img.png);"></div>
                    <div class="music__element__container">
                        <div class="music__name">01.  Начало работы с Treelone</div>
                        <div class="music__check learned"></div>
                        <div class="music__time">
                            <span>1:12 |</span>
                            <span> 3:22</span>
                        </div>
                        <div class="music__equalizer">
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                        </div>
                        <div class="music__menu">
                        
                            <div class="music__menu__popup">
                                <div class="music__menu__exit"></div>
                                <div class="music__menu__popup__title">Подать жалобу</div>
                                <ul>
                                    <li data-content>1. Контент для взрослых</li>
                                    <li>2. Нарушение авторских прав</li>
                                    <li>3. Экстремистские материалы</li>
                                    <li>4. Оскорбление</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="music__element active">
                    <div class="music__logo" style="background-image: url(./images/add_modules_img.png);"></div>
                    <div class="music__element__container">
                        <div class="music__name">01.  Начало работы с Treelone</div>
                        <div class="music__check learned"></div>
                        <div class="music__time">
                            <span>1:12 |</span>
                            <span> 3:22</span>
                        </div>
                        <div class="music__equalizer">
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                        </div>
                        <div class="music__menu">
                        
                            <div class="music__menu__popup">
                                <div class="music__menu__exit"></div>
                                <div class="music__menu__popup__title">Подать жалобу</div>
                                <ul>
                                    <li data-content>1. Контент для взрослых</li>
                                    <li>2. Нарушение авторских прав</li>
                                    <li>3. Экстремистские материалы</li>
                                    <li>4. Оскорбление</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="music__element active">
                    <div class="music__logo" style="background-image: url(./images/add_modules_img.png);"></div>
                    <div class="music__element__container">
                        <div class="music__name">01.  Начало работы с Treelone</div>
                        <div class="music__check learned"></div>
                        <div class="music__time">
                            <span>1:12 |</span>
                            <span> 3:22</span>
                        </div>
                        <div class="music__equalizer">
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                        </div>
                        <div class="music__menu">
                        
                            <div class="music__menu__popup">
                                <div class="music__menu__exit"></div>
                                <div class="music__menu__popup__title">Подать жалобу</div>
                                <ul>
                                    <li data-content>1. Контент для взрослых</li>
                                    <li>2. Нарушение авторских прав</li>
                                    <li>3. Экстремистские материалы</li>
                                    <li>4. Оскорбление</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="music__element active">
                    <div class="music__logo" style="background-image: url(./images/add_modules_img.png);"></div>
                    <div class="music__element__container">
                        <div class="music__name">01.  Начало работы с Treelone</div>
                        <div class="music__check learned"></div>
                        <div class="music__time">
                            <span>1:12 |</span>
                            <span> 3:22</span>
                        </div>
                        <div class="music__equalizer">
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                        </div>
                        <div class="music__menu">
                        
                            <div class="music__menu__popup">
                                <div class="music__menu__exit"></div>
                                <div class="music__menu__popup__title">Подать жалобу</div>
                                <ul>
                                    <li data-content>1. Контент для взрослых</li>
                                    <li>2. Нарушение авторских прав</li>
                                    <li>3. Экстремистские материалы</li>
                                    <li>4. Оскорбление</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="music__element active">
                    <div class="music__logo" style="background-image: url(./images/add_modules_img.png);"></div>
                    <div class="music__element__container">
                        <div class="music__name">01.  Начало работы с Treelone</div>
                        <div class="music__check"></div>
                        <div class="music__time">
                            <span>1:12 |</span>
                            <span> 3:22</span>
                        </div>
                        <div class="music__equalizer">
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                        </div>
                        <div class="music__menu">
                        
                            <div class="music__menu__popup">
                                <div class="music__menu__exit"></div>
                                <div class="music__menu__popup__title">Подать жалобу</div>
                                <ul>
                                    <li data-content>1. Контент для взрослых</li>
                                    <li>2. Нарушение авторских прав</li>
                                    <li>3. Экстремистские материалы</li>
                                    <li>4. Оскорбление</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="music__element active">
                    <div class="music__logo" style="background-image: url(./images/add_modules_img.png);"></div>
                    <div class="music__element__container">
                        <div class="music__name">01.  Начало работы с Treelone</div>
                        <div class="music__check learned"></div>
                        <div class="music__time">
                            <span>1:12 |</span>
                            <span> 3:22</span>
                        </div>
                        <div class="music__equalizer">
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                            <div class="music__equalizer__element"></div>
                        </div>
                        <div class="music__menu">
                        
                            <div class="music__menu__popup">
                                <div class="music__menu__exit"></div>
                                <div class="music__menu__popup__title">Подать жалобу</div>
                                <ul>
                                    <li data-content>1. Контент для взрослых</li>
                                    <li>2. Нарушение авторских прав</li>
                                    <li>3. Экстремистские материалы</li>
                                    <li>4. Оскорбление</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
        <?php include_once './components/music_mobile.php'?>
    </div>

<?php include_once './footer_application.php'; ?>
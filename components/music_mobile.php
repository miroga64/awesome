<div class="music__mobile__bg"></div>
<div class="music__mobile">
    <div class="music__mobile__title">
        <div class="music__mobile__title__arrow"></div>
        <div class="music__mobile__title__text">
            <div class="music__mobile__title__name">Начало работы с Treelone</div>
            <div class="music__mobile__title__subname">Знакомство с инструментами серв</div>
        </div>
        <div class="music__mobile__title__exit"></div>
    </div>
    <div class="music__mobile__container">
        <div class="music__mobile__logo"></div>
        <div class="music__mobile__name">Начало работы с Treelone</div>
        <div class="music__mobile__subname">Знакомство с инструментами серв</div>
        <div class="music__mobile__count">
            <div class="music__mobile__count__full"></div>
            <div class="music__mobile__count__now"></div>
        </div>
        <div class="music__mobile__time">
            <div class="music__mobile__time__element">1:12</div>
            <div class="music__mobile__time__element">3:22</div>
        </div>
        <div class="music__mobile__control">
            <div class="music__mobile__arrow"></div>
            <div class="music__mobile__play"></div>
            <div class="music__mobile__arrow rotate"></div>
            <div class="music__mobile__menu"></div>
        </div>
    </div>
</div>
<div class="add_modules add">
    <div class="add_modules__container" id="container2">
        <div class="add_modules__cross"></div>
        <div class="add_modules__title">Новые модули</div>
        <div class="add_modules__subtitle">Твои купленные модули. Кликни на нужный и он будет добавлен на карту. </div>
        <div class="add_modules__slider">
            <div class="add_modules__element" data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
        </div>
        <div class="add_modules__title">Старые модули</div>
        <div class="add_modules__subtitle">Модули, которые ты прошел. Модуль исчезает из карты и попадает сюда.</div>
        <div class="add_modules__slider">
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
            <div class="add_modules__element"  data-image="url(./images/skills_icon.svg)">
                <div class="add_modules__image" style="background-image: url(./images/add_modules_img.png)"></div>
                <div class="add_modules__name">Figma: основы для ди…</div>
                <div class="add_modules__categories">Дизайн</div>
            </div>
        </div>
    </div>
    <div class="add_modules__bg"></div>
</div>
<div class="side_menu">
    <div class="side_menu__logo"></div>
    <div class="side_menu__nav">
        <a class="side_menu__element" style="background-image:url(./images/side_play.svg)"></a>
        <a class="side_menu__element" style="background-image:url(./images/side_magnifire.svg)"></a>
        <a class="side_menu__element" style="background-image:url(./images/side_ring.svg)"></a>
        <a class="side_menu__element" style="background-image:url(./images/side_bell.svg)"></a>
    </div>
    <div class="side_menu__avatar" style="background-image:url(./images/side_avatar.svg)"></div>
</div>
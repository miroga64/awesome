<div class="add_modules menu">
    <div class="add_modules__container">
        <div class="add_modules__cross"></div>
        <div class="add_modules__title">Начало работы в Treelone</div>
        <ul>
            <li class='active'><a>Руководство по базе Photoshop для дизайнеров и ...</a></li>
            <li><a>Знакомство с инструментарием Photoshop</a></li>
            <li><a>Панель инструментов</a></li>
            <li><a>Color Picker</a></li>
            <li><a>Работа со слоями</a></li>
            <li><a>Вспомогательные элементы</a></li>
            <li><a>Возможность изменения уже созданного документа</a></li>
            <li><a>Сохранение документов</a></li>
        </ul>
    </div>
    <div class="add_modules__bg"></div>
</div>